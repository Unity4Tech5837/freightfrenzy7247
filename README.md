## NOTICE

This repository contains a customized version of the FTC SDK for the Freight Frenzy (2021-2022) competition season, customized by Team #7247 The H2O Loo Bots. Find the original [FTC SDK here](https://github.com/FIRST-Tech-Challenge/FtcRobotController).

## Getting Started
If you are new to robotics or new to *FIRST* Tech Challenge, then you should consider reviewing the FTC Blocks Tutorial to get familiar with how to use the control system.

[FTC Blocks Tutorial](https://github.com/FIRST-Tech-Challenge/FtcRobotController/wiki/Blocks-Tutorial)

[Android Studio Tutorial](https://github.com/FIRST-Tech-Challenge/FtcRobotController/wiki/Android-Studio-Tutorial)

# Release Notes

### These will be updated weekly on Thursdays

## 11.11.2021

* Added Lift Hinge to angle our intake arm
* Fixed TelemetryControl.java

## 24.10-09.11.2021

No change, didn't work on code

## 17-21.10.2021

* Replaced Lift Servo with Lift Motor
* Added simple Autonomous
* Refined Telemetry and further improvement to make the Duck Motor work in more distances from the origin of the turntable.
* Updated README.md

## 14.10.2021

* Added Duck Motor that can spin the Duck off if it is in the near center of the turntable.

## Initial Commit

* Created GitHub Repository, already built TeleOp OpMode with the resources to create an Autonomous OpMode.
